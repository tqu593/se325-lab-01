package se325.lab01.concert.server;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;

/**
 * Simple Java RMI server that creates a remotely accessible ConcertService
 * object and registers it with RMI's Naming service.
 */
public class Server {

    public static void main(String[] args) {
        try {

            // Create the Registry on the localhost.
            Registry lookupService = LocateRegistry.createRegistry(Config.REGISTRY_PORT);

            // Instantiate ShapeFactoryServant.
            ConcertService service = new ConcertServiceServant();

            // Advertise the ShapeFactory service using the Registry.
            lookupService.rebind(Config.SERVICE_NAME, service);

            Keyboard.prompt("Press Enter to shutdown the server ");
            lookupService.unbind(Config.SERVICE_NAME);

            System.out.println(
                    "The Concert service is no longer bound in the RMI registry. Waiting for lease to expire.");

        } catch (RemoteException e) {
            System.out.println("Unable to start or register proxy with the RMI Registry");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.out.println("Unable to remove proxy from the RMI Registry");
        }
    }
}