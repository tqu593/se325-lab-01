package se325.lab01.concert.common;

public class Config {

	public static final int REGISTRY_PORT = 8080;
	public static final String SERVICE_NAME = "concert-service";
}
