package se325.lab01.concert.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;

/**
 * JUnit test client for the RMI concert service application.
 */
public class Client {

	// Proxy object to represent the remote ShapeFactory service.
	private static ConcertService proxy;

	/**
	 * One-time setup method to retrieve the ShapeFactory proxy from the RMI
	 * Registry.
	 */
	@BeforeClass
	public static void getProxy() {
		try {
			// Instantiate a proxy object for the RMI Registry, expected to be
			// running on the local machine and on a specified port.
			Registry lookupService = LocateRegistry.getRegistry("localhost", Config.REGISTRY_PORT);

			// Retrieve a proxy object representing the Concert service.
			proxy = (ConcertService) lookupService.lookup(Config.SERVICE_NAME);
			
			System.out.println("Proxy ConcertService instantiated.");
		} catch (RemoteException e) {
			System.out.println("Unable to connect to the RMI Registry");
		} catch (NotBoundException e) {
			System.out.println("Unable to acquire a proxy for the Concert service");
		}
	}
	
	/**
	 * Resets the ConcertService proxy class for the next test.
	 */
	@After
	public void resetProxy() {
		try {
			proxy.clear();
		} catch (RemoteException e) {
			System.out.println("Unable to connect to the RMI Registry");
		}
	}
	
	/**
     * Tests that the server processes a Create request correctly.
     */
    @Test
    public void testCreate() throws IOException, ClassNotFoundException {
        Concert newConcert = new Concert("One Night of Queen", LocalDateTime.of(2017, 8, 4, 20, 0));
        Concert createdConcert = proxy.createConcert(newConcert);
        
        assertTrue(createdConcert.getTitle().equals("One Night of Queen") && 
        		createdConcert.getDate().equals(LocalDateTime.of(2017, 8, 4, 20, 0)));
    }

    /**
     * Tests that the server responds correctly to a Retrieve request.
     */
    @Test
    public void testFindWithExistingId() throws IOException, ClassNotFoundException {
        // Create and store new Concert object.
        Concert concert = new Concert("The Selecter and the Beat", LocalDateTime.of(2018, 1, 25, 20, 0));
        Concert createdConcert = proxy.createConcert(concert);
        
        // Read the response.
        Long id = createdConcert.getId();
        
        // Make a Retrieve request.
        Concert retrievedConcert = proxy.getConcert(id);
        
        assertEquals(retrievedConcert, createdConcert);
    }

    /**
     * Tests that the server responds correctly to a Retrieve request for a
     * non-existing Concert.
     */
    @Test
    public void testFindWithoutExistingId() throws IOException, ClassNotFoundException {
        // Make a Retrieve request.
        Concert retrievedConcert = proxy.getConcert(-1L);
        
        assertEquals(retrievedConcert, null);
    }

    /**
     * Tests that the server updates an existing Concert as expected.
     */
    @Test
    public void testUpdate() throws IOException, ClassNotFoundException {
        // Create and store new Concert object.
        Concert concert = new Concert("Spend the Night with Alice Cooper", LocalDateTime.of(2017, 10, 27, 19, 0));
        Concert createdConcert = proxy.createConcert(concert);

        // Attempt to update the Concert.
        createdConcert.setDate(LocalDateTime.of(2017, 10, 28, 19, 0));
        boolean updateSuccess = proxy.updateConcert(createdConcert);

        assertTrue(updateSuccess);
    }

    /**
     * Tests that the server deletes an existing Concert.
     */
    @Test
    public void testDelete() throws IOException, ClassNotFoundException {
        // Create and store new Concert object.
        Concert concert = new Concert("The Selecter and the Beat", LocalDateTime.of(2018, 1, 25, 20, 0));
        Concert createdConcert = proxy.createConcert(concert);

        // Attempt to delete the Concert.
        boolean deleteSuccess = proxy.deleteConcert(createdConcert.getId());

        assertTrue(deleteSuccess);
    }

    /**
     * Tests that the server responds correctly to a request to delete a non-
     * existent Concert.
     */
    @Test
    public void testDeleteWithInvalidId() throws IOException, ClassNotFoundException {
        // Attempt to delete the Concert.
        boolean deleteSuccess = proxy.deleteConcert(-20L);

        assertFalse(deleteSuccess);
    }

    /**
     * Tests that the server returns an empty List when it doesn't have any
     * Concerts.
     */
    @Test
    public void testListWithEmptyServer() throws IOException, ClassNotFoundException {
        List<Concert> concertList = proxy.getAllConcerts();
        assertEquals(0, concertList.size());
    }

    /**
     * Tests that the server returns a List containing all Concerts stored by
     * the server.
     */
    @Test
    public void testList() throws IOException, ClassNotFoundException {
        // Create and store new Concert object.
        Concert concert = new Concert("One Night of Queen", LocalDateTime.of(2017, 8, 4, 20, 0));
        Concert createdConcert = proxy.createConcert(concert);

        List<Concert> concertList = proxy.getAllConcerts();
        
        assertEquals(1, concertList.size());
    }

}
